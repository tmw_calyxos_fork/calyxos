# unofficial calyxos fork (tmw_calyxos_fork)

The tmw_calyxos_fork project group represents an unofficial fork of CalyxOS which attempts to add support for the OnePlus 5, an older device that no longer receives security updates and does not qualify for official support. In addition, it endeavors to add other features that this author finds useful, such as additional support/workarounds for multiple work profiles, implemented in a hackish way. You are free to use these projects in accordance with their original licenses, but your mileage may vary. As of this writing, the author is the only person utilizing any of their code modifications, and support or assistance may not be available.

This specific GitLab project is meant for issues related to this unofficial CalyxOS fork. This repo is forked from CalyxOS's own calyxos repo, which may not have been necessary, but so it goes. The rest of this readme (and project) remains unmodified. Because it's only meant for issue tracking, merges from the upstream repo are not guaranteed.

Central place to hold documentation about the project.

## Changelogs

The [changelogs](https://gitlab.com/calyxos/calyxos/tree/master/changelogs) directory contains changelogs for the CalyxOS builds.

The [current.md](https://gitlab.com/calyxos/calyxos/blob/master/changelogs/current.md) file will always contain the changelog for the upcoming build, and can be empty sometimes right after a release.

The other files contain the changelog for that particular build.

The [current.md](https://gitlab.com/calyxos/calyxos/blob/master/changelogs/current.md) file is renamed to the build number after a build is released.

## wiki

The [wiki](https://gitlab.com/calyxos/calyxos/wikis/home) is a good starting point and contains lots of useful information.
